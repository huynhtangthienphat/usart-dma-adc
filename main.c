#include "USART.h"
#include "dma.h"
#include "delay.h"
#include "function_millis.h"
#include "adc_dma.h"
unsigned long now=0;
unsigned long taskOne=0;
int main(void)
{
	SetSystem72Mhz();
	ConfigureTimer3();
	char buffer[5];
	USART_Config();
	USARTx_DMA_Config();
	USART_DMACmd(DEBUG_USARTx,USART_DMAReq_Tx,ENABLE);
	SendBuff[4]='\r';
	SendBuff[5]='\n';
	RCC->APB2ENR |= (1<<4);
	GPIOC->CRH = 0x44344444;
	ADCx_Init();
	
	delay_init();
	while(1)
	{
		if(millis()-now>=100)
		{
			now=millis();
			sprintf(buffer,"%d",ADC_ConvertedValue[0]);
			int j=0;
			while(buffer[j]!='\0')
			{
				SendBuff[j]=buffer[j];
				j++;
			}
			USARTx_DMA_Restart();
		}
		if(millis()-taskOne>=1000)
		{
			taskOne=millis();
			GPIOC->ODR ^= (1<<13);
		}
	}
}