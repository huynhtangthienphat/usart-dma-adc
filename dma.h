#include "stm32f10x.h"                  // Device header
#include "stm32f10x_dma.h"              // Keil::Device:StdPeriph Drivers:DMA

#define USART_TX_DMA_CHANNEL DMA1_Channel4

#define USART_DR_ADDRESS ((u32)&USART1->DR)
#define SENDBUFF_SIZE 6
extern uint8_t SendBuff[SENDBUFF_SIZE];
void USARTx_DMA_Config(void);
void USARTx_DMA_Restart(void);
